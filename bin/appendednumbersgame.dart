var steps = {};

void main(a)
{
    c(a[0], int.parse(a[1]));
}

void c(String input, int max)
{
    int i = 0;
    int n = int.parse(input);
    
    while(++i <= max)
    {
        n = addUp(n.toString());

        steps[i] = n;
        
        if(n < 10)
            break;
    }
    
    printSteps();
}

int addUp(String n)
{
    List numbers = [];
    
    for(int i = 0; i < n.length; i++)
    {
        try
        {
            numbers.add(n[i] + n[i + 1]);
        }
        catch(e){}
    }
    
    return addNumbers(numbers);
}

int addNumbers(List numbers)
{
    Iterator it = numbers.iterator;

    String s = "";
    
    while(it.moveNext())
    {
        int i = 0;
        for(String s in it.current.split(''))
        {
            i += int.parse(s);
        }
                
        s = s + i.toString();
    }
    
    return int.parse(s);
}

void printSteps()
{
    int l = steps.length;
    
    for(int i = getStart(l); i <= l; i++)
    {        
        print("${i}:\t${steps[i]}");
    } 
}

int getStart(int l)
{
    int m = l - 4;
    return m > 0 ? m : 1;
}